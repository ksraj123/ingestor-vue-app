export default [{
  path: '/projects',
  alias:['/', '/login'],
  name: 'MyProjects',
  meta :{
    requiresAuth:true
  },
  component: () => import('../views/MyProjects.vue')
}, {
  path: '/project/creation',
  name: 'NewProject',
  meta :{
    requiresAuth:true
  },
  component: () => import('../views/NewProject.vue')
}, {
  path: '/project/edit/:projectId',
  name: 'EditProject',
  meta :{
    requiresAuth:true
  },
  component: () => import('../views/NewProject.vue')
}]
