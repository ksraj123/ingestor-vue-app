export default [{
  path: '/ilmOld/:projectId',
  name: 'IlmProjectOld',
  meta :{
    requiresAuth: true
  },
  component: () => import('../views/Project.vue')
}, {
  path: '/ilmOld/export/:projectId',
  name: 'ExportIlmProjectOld',
  meta :{
    requiresAuth: true,
    showNavbar: false
  },
  component: () => import('../../intelligentLiteratureMonitoringUpdate/views/ExportArticleList.vue')
}, {
  path: '/ilmOld/:projectId/:articleId',
  name: 'IlmArticleOld',
  meta :{
    requiresAuth: true
  },
  component: () => import('../views/Article.vue')
}]