export default [{
  path: '/create-instance',
  name: 'CreateInstance',
  meta: {
    title: 'CreateInstance',
    requiresAuth: false,
    showNavbar: false
  },
  component: () => import('../views/CreateInstance.vue')
}]
