const months = {
  1: "JAN",
  2: "FEB",
  3: "MAR",
  4: "APR",
  5: "MAY",
  6: "JUN",
  7: "JUL",
  8: "AUG",
  9: "SEP",
  10: "OCT",
  11: "NOV",
  12: "DEC"
}

// Clones simple objects and arrays
const clone = object => {
  if (object instanceof Array) {
    return object.map(clone)
  }
  if (object && object instanceof Object) {
    const newObject = {}
    Object.entries(object).forEach(([prop, value]) => {
      newObject[prop] = clone(value)
    })
    return newObject
  }
  return object
}

const api = {

  copyToClipboard(text) {
    const textElem = document.createElement('textarea')
    textElem.value = text
    document.body.appendChild(textElem)
    textElem.select()
    document.execCommand('copy')
    document.body.removeChild(textElem)
  },

  goToExternalUrl(url) {
    window.location.replace(url)
  },

  getTimeValues (inputDate = new Date()) {
    // Note: time is always converted into UTC
    const [dateRaw, timeRaw] = new Date(inputDate).toISOString().split('T')
    const [year, month, day] = dateRaw.split('-').map(v => parseInt(v, 10))
    const [hour, minute, second] = timeRaw.split('.')[0].split(':').map(n => +n)
    return { year, month: month - 1, day, hour, minute, second }
  },

  formatDate(date, includeYear) {
    const { year, month, day } = this.getTimeValues(date)
    return `${months[month + 1]} ${`${day}`.padStart('0', 2)}${includeYear ? ` ${year}` : ''}`
  },

  formatDatePickerFormat (date, options) {
    const { showDuplicates } = (options || {})
    return date
        .split('-')
        .filter((n, i, a) => (
          n &&
          // Don't show a range if the to and from points are the same
          // (only hide duplicate dates if the calendar is closed)
          (showDuplicates || i !== 1 || a[0] !== n)
        ))
        .map(dateName => {
        const { day } = api.getTimeValues(dateName)
        const monthStr = new Date(dateName).toLocaleString(navigator.language, { month: 'short' })
        return `${monthStr} ${day}`
      }).join(' - ') || ''
  },

  getRageDatePreview(startDate = null, endDate = null) {
    var datePreview = ''
    if (startDate) {
      datePreview = this.formatDate(startDate)
    }
    if (endDate) {
      endDate = this.formatDate(endDate)
      let today = this.formatDate(new Date())
      let end = endDate == today ? 'TODAY' : endDate
      datePreview += ` - ${ end }`
    }
    return datePreview
  },
  stringToColour(str) {
    var hash = 0;
    for (var i = 0; i < str.length; i++) {
      hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }
    var colour = '#';
    for (i = 0; i < 3; i++) {
      var value = (hash >> (i * 8)) & 0xFF;
      colour += ('00' + value.toString(16)).substr(-2);
    }
    return colour;
  },
  getRGBA(hexColor) {
    console.log(hexColor)
    if (!hexColor) {
      hexColor = "#888888"
    }
    let hex = hexColor.replace('#', '')
    const r = parseInt(hex.substring(0, 2), 16)
    const g = parseInt(hex.substring(2, 4), 16)
    const b = parseInt(hex.substring(4, 6), 16)
    return `rgba(${r}, ${g}, ${b}, 0.5)`
  },

  clone,

  createCitation ({
    authors: input_authors,
    doi: input_doi,
    data_source,
    title,
    journal = '',
    journal_volume,
    journal_issue,
    pagination: input_pagination,
    provider_id,
    publication_date
  }) {
    const authors = (() => {
      let formattedAuthorNames = []
      let names = input_authors.map(author => author.name)
      names.forEach(name => {
        if(!name.includes(",")){
          formattedAuthorNames.push(name)
        } else{
          let last_name, first_name, middle_name;
          [last_name, first_name] = name.split(",");
          [first_name, middle_name] = first_name.trim().split(" ")
          middle_name = middle_name ? middle_name : ""
          let formatted = last_name + " " + first_name.charAt(0) + middle_name.charAt(0)
          formattedAuthorNames.push(formatted)
        }
      })
      return formattedAuthorNames.join(', ')
    })()
    const publicationDate = publication_date ? publication_date: ""
    const volume = journal_volume ? journal_volume : ""
    const issue = journal_issue ? `(${journal_issue})` : ""
    const pagination = input_pagination ? `:${input_pagination}` : ""
    const doi = input_doi ? `doi: ${input_doi}` : ""
    const providerId = provider_id ? `PMID: ${provider_id}.` : ""

    const sep = (str, sep, ...rest) => {
      const restStr = rest.join('')
      return str
        ? restStr
          ? `${str}${sep}${restStr}`
          : str
        : restStr
    }

    switch(data_source?.toLowerCase?.()) {
      // https://libguides.jcu.edu.au/ama/what-if/no-volume-issue-number
      case 'pubmed':
        return `${ authors }. ${ title }. ${ journal }.
          ${ sep(publicationDate, ';', `${ volume }${ issue }`) }${ pagination }. ${ doi }.
          ${ providerId }`.replace(/\s+/g,' ').trim()
      // https://www.biorxiv.org/submit-a-manuscript
      case 'biorxiv':
        return `${ authors }. ${ title }. ${ "bioRxiv. " }
          ${ sep(publicationDate, '. ', doi) }.`.replace(/\s+/g,' ').trim()

      case 'medrxiv':
        return `${ authors }. ${ title }. ${ "medRxiv. " }
          ${ sep(publicationDate, '. ', doi) }.`.replace(/\s+/g,' ').trim()

      default:
        return ''
    }
  }
}

export default api