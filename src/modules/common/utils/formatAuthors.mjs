export default ({ authors, extended = false, maxLength = 40 }) => {
  // Names are assumed to be in these formats from the server:
  // Lastname, Firstname Middlename
  // Lastname, Firstname M
  // Lastname, F M
  // The desired format is:
  // Lastname FM
  const formattedAuthors = authors
    .map(({ name }) => name)
    .map(name => {
      const [last, firstMiddle] = name.split(',').map(n => n.trim())
      const [first, middle] = (firstMiddle || '').split(' ').map(n => n.trim())
      return `${last} ${first}${(middle || '')[0] || ''}`
    })

  // Filter out authors that cause the charlength to be greater than the max
  const filteredAuthors = formattedAuthors.reduce(
    ({ allAuthors, filteredAuthors }, author, index) => ({
      allAuthors: allAuthors.concat(author),
      filteredAuthors: (
        index === 0 ||
        extended ||
        `${allAuthors.join(', ')} ${author}`.length < maxLength
      )
        ? filteredAuthors.concat(author)
        : filteredAuthors
    }),
    { allAuthors: [], filteredAuthors: [] }
  ).filteredAuthors

  return {
    formattedAuthors: filteredAuthors.join(', ').trim(),
    extras: formattedAuthors.length - filteredAuthors.length
  }
}