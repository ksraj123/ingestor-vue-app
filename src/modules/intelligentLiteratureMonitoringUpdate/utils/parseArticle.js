// TODO: replace ".js" with ".mjs" once this is merged:
// https://gitlab.com/ingestor/app-services/ingestor-vue-app/-/merge_requests/157
import Tools from '@/modules/common/utils/Tools.mjs'

export default ({ projectId, article, existingArticle = {} }) => {
  const articleId = article.article_id

  return {
    authors: article.authors,
    articleId,
    articleLink: `/ilm/${projectId}/${articleId}`,
    articleLinks: article.article_links,
    bookmarked: article.starred_article,
    citation: Tools.createCitation(article),
    excerpt: article.preview_excerpt,
    publicationDate: (() => {
      const d = new Date(article.publication_date)
      const { day, year } = Tools.getTimeValues(d)
      d.setDate(1)
      const month = d.toLocaleString(navigator.language, { month: 'long' })
      return `${month} ${day}, ${year}`
    })(),
    journal: article.journal,
    isLoadingSpans: !existingArticle.spans && !article.spans && article.ontology_tag_chart.length > 0,
    ontologyTags: article.ontology_tag_chart.map(({ most_prevalent, ontology_name }) => ({
      keyword: most_prevalent,
      name: ontology_name
    })),
    ontologyTagChart: (() => (
      Object.values(article.ontology_tag_chart.reduce(
        (
          resultsHash,
          {
            keyword: keywordArray,
            ontology_name: name,
            total
          }
        ) => {
          const item = resultsHash[name] || {
            color: Tools.getRGBA(Tools.stringToColour(name)),
            name,
            synonyms: [],
            total: 0
          }

          return {
            ...resultsHash,
            [name]: {
              ...item,
              total: item.total + total,
              synonyms: item.synonyms.concat(
                keywordArray.map(({ total, keyword: name }) => ({
                  name,
                  total
                }))
              )
            }
          }
        },
        []
      ))
    ))(),
    publicationType: article.publication_type
      ? article.publication_type instanceof Array
        ? article.publication_type.join(', ') || article.type
        : article.publication_type
      : article.type,
    spans: (() => {
      if (!article.spans) {
        return existingArticle.spans || article.summary || ''
      }

      const spans = article.spans
        // Copy/simplify
        .map(({
          span_start_character,
          in_text,
          ontology_name,
          predictionScore
        }) => ({
          start: span_start_character,
          end: span_start_character + in_text.length,
          color: Tools.getRGBA(Tools.stringToColour(ontology_name)),
          name: ontology_name,
          predictionScore
        }))
        // Sort by occurrence in summary
        .sort((a, b) =>
          a.start - b.start ||
          b.end - a.end
        )
        // Filter out overlapping tags (prefer larger ones)
        .reduce((addedSpans, span) =>
        addedSpans.every(addedSpan =>
          span.start >= addedSpan.end ||
          (span.end - span.start) >= (addedSpan.end - addedSpan.start)
        )
          ? addedSpans.concat(span)
          : addedSpans, [])
        // Filter out overlapping tags (prefer later ones)
        .reduceRight((addedSpans, span) =>
          addedSpans.every(addedSpan =>
            span.end < addedSpan.start ||
            addedSpan.end < span.start
          )
            ? [span].concat(addedSpans)
            : addedSpans, [])

      const rawSummary = article.summary

      const newSummary = spans.length
        ? spans.reduce((summary, span, index) => {
          const spanEl = document.createElement('span')
          spanEl.innerText = rawSummary.substring(span.start, span.end)
          spanEl.style = [
            `background-color: ${span.color}`,
            `border-radius: 5px`,
            `padding: 1px 3px`
          ].join(';')
          spanEl.title = span.name

          return summary.concat([
            spanEl.outerHTML,
            rawSummary.substring(
              spans[index].end,
              index < spans.length - 1
              ? spans[index + 1].start
              : Infinity
            )
          ])
        }, [
          rawSummary.substring(0, spans[0].start)
        ])
        : [rawSummary]
      return newSummary.join('')
    })(),
    summary: article.summary,
    title: article.title
  }
}
