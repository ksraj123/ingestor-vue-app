import Repository from "@/modules/common/utils/Repository";

export default {
  searchArticles (projectId, filters = {}) {
    let filterData = {
      relevant: true,
      sort_order: "desc",
      size: 10,
      page: 1,
      ...filters
    }
    return Repository.ivdrApi.post(`projects/${projectId}/ivdr/search`, filterData)
  },
  getArticle (projectId, articleId) {
    return Repository.ivdrApi.get(`projects/${projectId}/ivdr/${articleId}`)
  },
  updateArticle (projectId, articleId, data) {
    return Repository.ivdrApi(
      {
        url: `projects/${projectId}/ivdr/${articleId}/ivdr_custom_fields`,
        method: 'post',
        data,
        headers: {
          'Content-Type': "application/json",
        }
      }
    )
  },
  downloadExcel (projectId) {
    return Repository.ivdrApi(
      {
        url: `projects/${projectId}/ivdr/export_articles_excel`,
        responseType: 'blob',
        headers: {
          accept: "application/json",
        }
      }
    )
      .then((res) => {
        let blob = res.data
        let reader = new FileReader()
        reader.readAsDataURL(blob)
        reader.onload = (e) => {
          let a = document.createElement('a')
          a.download = 'export.xlsx'
          a.href = e.target.result
          document.body.appendChild(a)
          a.click()
          document.body.removeChild(a)
        }
      })
  }
}
