export default [
  {
    path: '/ivdrOld/:projectId',
    name: 'IvdrProjectOld',
    meta :{
      requiresAuth: true
    },
    component: () => import('../views/Project.vue')
  },
  // {
  //   path: '/ivdr/export/:projectId',
  //   name: 'ExportIvdrProject',
  //   meta :{
  //     requiresAuth: true,
  //     showNavbar: false
  //   },
  //   component: () => import('../views/ExportArticleList.vue')
  // },
  // {
  //   path: '/ivdr/:projectId/:articleId',
  //   name: 'IvdrArticle',
  //   meta: {
  //     // requiresAuth: true
  //   },
  //   component: () => import('../views/Article.vue')
  // }
]