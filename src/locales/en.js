import { en } from 'vuetify/lib/locale'

export default {
  ...en,

  common: {
    misc: {
      loading: "Loading",
      noResults: "No results"
    },
    labels: {
      optional: "Optional",
      accept: "Accept",
      termsAndConditions: "Terms and Conditions",
      project: "Project",
      thankYou: "Thank You!",
      selectAll: "Select all"
    },
    buttons: {
      upload: "Upload",
      create: "Create",
      next: "Next",
      finish: "Finish",
      edit: "Edit",
      previousStep: "Previous Step",
      getStarted: "Get Started",
      clearAll: "Clear All",
      expandAll: "Expand All",
      collapseAll: "Collapse All",
      clearAllStars: "Clear stars"
    }
  },
  projects: {
    labels: {
      myProject: "My Projects",
      newProject: "New Project",
      editProject: "Edit Project",
      insights: "Insights",
      intelligentLiteratureMonitoring: "Intelligent Literature Monitoring",
      ivdrmdr: "IVDR/MDR",
      medicalGuidelines: "Medical Guidelines",
      medicalInformation: "Medical Information",
      medicalEducation: "Medical Education",
      addingNewProject: "Adding a new project takes less than 5 mins",
      comingSoon: "Coming Soon",
      banner: {
        content: "Content",
        areasOfInterest: "Areas of Interest",
        preferences: "Preferences",
        summary: "Auto-summary"
      },
      form: {
        name: "Project Name",
        description: "Project Description",
        simple: "Simple",
        predefinedKeywords: "Predefined Keywords",
        customKeywords: "Custom Keywords",
        advanced: "Advanced",
        dataSources: "Data Sources",
        keywordsArgs: "Keywords / Topics",
        keywordSubtext: "Add multiple keywords separated by semi-colon.",
        articlePublicationDate: "Article Publication Date",
        deliverySchedule: "Delivery Schedule",
        daily: "Daily",
        weekly: "Weekly",
        monthly: "Monthly",
        additionalInformation: "Additional information"
      },
      messages: {
        notificationEmail: "You will receive a notification email on each delivery date.",
        areasOfInterestDisclaimer: "Sorcero will generate custom ontologies and search strings based on the keywords shared. This will take 2-3 days",
        additionalInformation: "Please feel free to send any additional information or project requirements to the Sorcero team via email at "
      }
    },
    buttons: {
      addProject: "Add Project",
      viewReport: "View Report"
    }
  },
  login: {
    labels: {
      forgotPassword: "Forgot password?",
      doNotHaveAccount: "Don't have an account?",
      signUp: "Sign up",
      welcome: "Welcome !",
      labelHook: "Analyze Scientific Content at Scale",
      errorLogin: "Incorrect email or password",
      userAccount: {
        title: "Set up your user account",
        description: "You can add more team members after your account has been created."
      }
    },
    inputs: {
      email: "Email",
      password: "Password",
      workEmail: "Work email address *",
      setPassword: "Set a password *"
    },
    buttons: {
      login: "Login"
    }
  },
  createInstance: {
    labels: {
      about: {
        title: "About your company",
        description: "To get started, please tell us your company name so what we can create a dedicated workspace for your team members to create projects."
      },
      banner: {
        description: "Create your company account in less than 5 minutes",
        companyDetails: "Company details",
        userDetails: "User details",
        final: "You are all set!"
      },
      account: {
        createdMessage: "Your account has been created.",
        instructionsMessage: "We have emailed you a verification link. Unverified accounts are deactivated 48 hours after creation.",
        verifyMessage: "Please verify your email address for continued access."
      },
      companyName: "Company Name *",
      companyLink: "Your company instance link",
      companyLogo: "Company Logo",
      companyLogoMessage: "Personalise the app with your company branding.",
      passwordRequirements: "Min 8 characters,  alphanumeric"
    }
  },
  landingPage: {
    labels: {
      startTitle: "Scientific and Medical Literature remains an important source of information to identify suspected adverse reactions.",
      startSubtitle: "Sorcero helps you to reduce the effort in literature monitoring while minimising the risk of missing references. We use ML to determine the relevance of a reference, giving a ranking of between 1 and 0 to each article.",
      literatureSurveillance: "Literature Surveillance",
      literatureSurveillanceDescription: "A periodic scanning ot literature to detect when guideline recommendations may require change. There are 2 times in the standard process that literature surveillance is often performed: During guideline development and between versions of guidelines. In each case, the process for how to manage the literature is similar."
    },
    buttons: {
      startTrial: "Start your Trial"
    }
  },
  ilm: {
    labels: {
      starredArticlesOnly: "Starred articles only",
      articles: "Articles",
      articleLinks: "Article Link/s",
      showLess: "show less",
      more: "more",
      filters: "Filters",
      source: "Source",
      articleType: "Article Type",
      journal: "Journal",
      author: "Author",
      tag: "Tag",
      knowledgeGraph: "Knowledge Graph",
      backToSearchPage: "Back to Search Page",
      excerpt: "Excerpt",
      placeholderSource: "Select source/s",
      placeholderJournal: "Select journal/s",
      placeholderAuthor: "Select author/s",
      placeholderArticleType: "Select article type/s",
      placeholderTag: "Select tag/s",
      filtersApplied: "Filters Applied",
      noFiltersApplied: "No Filters Applied",
      datePublished: "Date Published",
      sortResultsBy: "Sort Results By",
      copyLink: "Copy Link",
      linkCopied: "Link copied"
    },
    buttons: {
      copyCitation: "Copy Citation"
    }
  },
  ivdr: {
    toast: {
      notVerified: "This account has not been verified. Please follow the instructions in your email to complete verification.",
      ingestionStatus: "We ingested $1 new $3 this week out of which we found $2 relevant $4.",
      article: "article",
      articles: "articles"
    },
    articles: {
      postMarketEvaluation: "Post Market Evaluation",
      postMarketEvaluationSubtext: "IVDR - In Vitro Diagnostic Regulation",
      relevantArticles: "Relevant Articles ($1)",
      nonRelevantArticles: "Non-Relevant Articles ($1)",
      synchronize: "Press this to synchronize the feedback shared and reflect the change in the page",
      tags: {
        analyteSubject: "Analyte Subject",
        humanStudy: "Human Study",
        performanceStudy: "Performance Study",
        intended: "Intended Use"
      },
      feedback: "Feedback noted and will be used for model training purposes. Article marked as $1.",
      relevant: "relevant",
      nonRelevant: "non-relevant"
    },
    labels: {
      copyLink: "Copy Link",
      articleLinks: "DOI / Article Link(s) ($1)"
    },
    pagination: {
      resultsPerPage: "Results per page",
      pageOf: "Page $1 of $2"
    }
  },
  ilmUpdate: {
    articleLinks: "Article Links",
    aToZ: "A to Z",
    authors: "Authors",
    authorsTooltip: "Author(s)",
    autoSummary: "Auto-summary",
    backToProjects: "Back to projects",
    bookmarkArticle: "Bookmark Article",
    bookmarkedArticlesOnly: "Bookmarked Articles Only",
    categories: "Categories",
    citationCopied: "Citation Copied",
    clearFilters: "Clear Filters",
    contractAll: "Contract All",
    copyCitation: "Copy Citation",
    copyLinkTooltip: "Copy to clipboard",
    copyLink: "Copy Link",
    date: "Date",
    dateRange: "Date Range",
    expandAll: "Expand All",
    exportData: "Export Data",
    exportDataTooltip: "Export data to .xlsx",
    filters: "Filters",
    hideFilter: "Hide Filter",
    journals: "Journals",
    journalTooltip: "Journal",
    knowledgeGraph: "Knowledge Graph",
    linkCopied: "Link Copied",
    loading: "Loading...",
    newestFirst: "Newest First",
    noResults: "No results",
    ofNumArticles: "of $1 articles",
    oldestFirst: "Oldest First",
    ontologyTags: "Tags",
    publicationDateTooltip: "Date of publication",
    publicationTypes: "Publication Types",
    removeBookmark: "Remove Bookmark",
    save: "Save",
    showFilter: "Show Filter",
    sortBy: "Sort by",
    sources: "Sources",
    sourceTypeTooltip: "Source type",
    showLess: "Less",
    showMore: "More",
    title: "Title",
    viewLess: "View Less",
    viewMore: "View More",
    zToA: "Z to A"
  },
  ivdrUpdate: {
    analyteSubject: "Analyte Subject",
    articleLinks: "Article Link(s)",
    authorsTooltip: "Author(s)",
    bookmarkArticle: "Bookmark Article",
    completeTagging: "Complete Tagging",
    confirmTagging: "Confirm Tagging",
    contractAll: "Contract All",
    citationCopied: "Citation Copied",
    copyCitation: "Copy Citation",
    copyLink: "Copy Link",
    copyLinkTooltip: "Copy to clipboard",
    expandAll: "Expand All",
    exportData: "Export Data",
    exportDataTooltip: "Export data to .xlsx",
    feedbackProvidedForTraining: "Feedback has been provided to model for training.",
    filter: "Filter",
    humanStudy: "Human Study",
    incompleteChanges: "You have incomplete article category changes",
    intendedUse: "Intended Use",
    journalTooltip: "Journal",
    loading: "Loading",
    loseChanges: "Lose Changes",
    movedToNotRelevant: "Moved to non-relevant.",
    movedToRelevant: "Moved to relevant.",
    notRelevant: "Not Relevant",
    notRelevantTooltip: "Articles not satisfying one or more criteria",
    performanceStudy: "Performance Study",
    publicationDateTooltip: "Date of publication",
    relevant: "Relevant",
    relevantTooltip: "Articles satisfying all criteria",
    showLess: "Show Less",
    showMore: "Show More",
    sourceTypeTooltip: "Source type",
    undo: "Undo",
    userModifiedTooltip: "This field has been changed by user",
    viewLess: "View Less",
    viewMore: "View More"
  }
}
