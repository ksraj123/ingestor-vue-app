export default {
  primary: '#1d8755', // Primary, Secondary, Tertiary - Buttons, Breadcrumbs
  secondary: '#09609f', // Left and Top Navigation Header, Alert
  accent: '',
  error: '#d72C0e', // Error in input fields, Error message alert
  warning: '#ffb045', // Warning alert, Star
  info: '#2180ff', // Info message alert
  success: '#3db119' // Success message alert
}