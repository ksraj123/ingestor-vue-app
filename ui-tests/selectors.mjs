// Basic selectors go here
export const s = {
  // These are not official selectors
  loginUserInput: '#login-email',
  loginPassInput: '#login-password',
  loginButton: '#login-button',
  loginInvalidError: 'text=Incorrect email or password',
  headerLogoButton: '#home-link',
  headerMenuButton: '#home-account-menu',
  logoutMenuItem: '#home-account-menu-logout',
  viewReportIlmLink: ':nth-match(:text("View Report"), 1)'
}

export const getSelectors = page => Object.entries(s).reduce((api, [prop, selector]) => ({
  ...api,
  [prop]: () => page.locator(selector)
}), {
  // Any elaborate Testwright-specifc selectors go here, ex:
  // () => page.locator('.abc').first()
})
