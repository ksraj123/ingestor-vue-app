import { test, expect } from '@playwright/test';
import { s, getSelectors } from '../selectors.mjs'
import { hostUrl } from '../constants.mjs'

console.log('\n\x1b[1mRememenber to set VUE_APP_AUTH0_DOMAIN=mock-server in your env file\x1b[0m\n')

test('Header button navigates back to home', async ({ page }) => {
  await page.goto(hostUrl)

  await page.click(s.viewReportIlmLink)
  await page.waitForLoadState('networkidle')
  await expect(page).toHaveURL('http://localhost:8080/ilm/pxrojectid0?filter=%257B%2522sort_order%2522%3A%2522desc%2522,%2522sort_field%2522%3A%2522publication_date%2522%257D')
  await page.click(s.headerLogoButton)
  await page.waitForLoadState('networkidle')
  await expect(page).toHaveURL('http://localhost:8080')

});
