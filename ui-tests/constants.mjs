// For now these are not configurable,
// but these tests aren't going to run against different environments.

export const hostUrl = 'http://localhost:8080/'
export const loginUrl = `${hostUrl}login`
export const projectsUrl = `${hostUrl}projects`
