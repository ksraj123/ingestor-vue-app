const path = require('path')
const express = require('express')

const port = 8080
const host = "0.0.0.0"
const app = express()

app.use(express.static(path.join(__dirname, '/dist')))

app.get('/*', function(req, res){
  res.sendFile("index.html", {root: path.join(__dirname, '/dist')})
})

app.listen(port, host, () => {
  console.log(`Server running http//${host}:${port}`)
})