#########################

FROM node:lts-alpine as core

WORKDIR /usr/src

RUN apk add git --no-cache

COPY package.json vue.config.js ./

ARG INGESTOR_PULL_TOKEN

RUN sed -i "s,git+ssh://git@gitlab.com:ingestor/,git+https://$INGESTOR_PULL_TOKEN@gitlab.com/ingestor/,g" package.json

RUN npm install

#########################

FROM node:lts-alpine as build

WORKDIR /usr/src
ARG DEPLOYMENT_TYPE=production

COPY . .
COPY .env.${DEPLOYMENT_TYPE} .env

COPY --from=core /usr/src/node_modules ./node_modules

RUN npm run build

FROM build as server

CMD node server.js

FROM nginx:1.18.0-alpine

COPY --from=build /usr/src/dist/ /usr/share/nginx/html

COPY nginx.conf /etc/nginx/conf.d/default.conf