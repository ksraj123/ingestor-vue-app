import assert from 'assert'

import Tools from '../src/modules/common/utils/Tools.mjs'

const { createCitation } = Tools

const pubMedArticle = {
  authors: [
    {
      affiliations: ['Affiliation 1', 'Affiliation 2'],
      name: 'AuthorA'
    },
    {
      affiliations: ['Affiliation 1', 'Affiliation 2'],
      name: 'AuthorB'
    }
  ],
  doi: '10.1010/j.ajoc.2020.010101',
  data_source: 'pubmed',
  title: 'Article Title',
  journal: 'Journal Name',
  journal_volume: 'Journal Volume',
  journal_issue: 'Journal Issue',
  pagination: '101234',
  provider_id: '12345678',       
  publication_date: '2022-01-31'
}

const bioRxivArticle = {
  ...pubMedArticle,
  data_source: 'bioRxiv'
}

const medRxivArticle = {
  ...pubMedArticle,
  data_source: 'medrxiv'
}

const omitProps = (article, propsToOmit) => Object.entries(article)
  .reduce((newObj, [prop, value]) => (
    propsToOmit.includes(prop)
      ? newObj
      : {
        ...newObj,
        [prop]: value
      }
  ), {})

describe('PubMed Citations', () => {
  it('Publication date, volume, issue, pagination', () => {
    assert.equal(
      createCitation(pubMedArticle),
      'AuthorA, AuthorB. Article Title. Journal Name. 2022-01-31;Journal Volume(Journal Issue):101234. doi: 10.1010/j.ajoc.2020.010101. PMID: 12345678.'
    )
  })
  it('Publication date, volume, pagination', () => {
    assert.equal(
      createCitation(omitProps(pubMedArticle, ['journal_issue'])),
      'AuthorA, AuthorB. Article Title. Journal Name. 2022-01-31;Journal Volume:101234. doi: 10.1010/j.ajoc.2020.010101. PMID: 12345678.'
    )
  })
  it('Publication date, volume, issue', () => {
    assert.equal(
      createCitation(omitProps(pubMedArticle, ['pagination'])),
      'AuthorA, AuthorB. Article Title. Journal Name. 2022-01-31;Journal Volume(Journal Issue). doi: 10.1010/j.ajoc.2020.010101. PMID: 12345678.'
    )
  })
  it('Publication date, issue, pagination', () => {
    assert.equal(
      createCitation(omitProps(pubMedArticle, ['journal_volume'])),
      'AuthorA, AuthorB. Article Title. Journal Name. 2022-01-31;(Journal Issue):101234. doi: 10.1010/j.ajoc.2020.010101. PMID: 12345678.'
    )
  })
  it('Publication date, pagination', () => {
    assert.equal(
      createCitation(omitProps(pubMedArticle, ['journal_issue', 'journal_volume'])),
      'AuthorA, AuthorB. Article Title. Journal Name. 2022-01-31:101234. doi: 10.1010/j.ajoc.2020.010101. PMID: 12345678.'
    )
  })
  it('Publication date, volume', () => {
    assert.equal(
      createCitation(omitProps(pubMedArticle, ['journal_issue', 'pagination'])),
      'AuthorA, AuthorB. Article Title. Journal Name. 2022-01-31;Journal Volume. doi: 10.1010/j.ajoc.2020.010101. PMID: 12345678.'
    )
  })
  it('Publication date, issue', () => {
    assert.equal(
      createCitation(omitProps(pubMedArticle, ['journal_volume', 'pagination'])),
      'AuthorA, AuthorB. Article Title. Journal Name. 2022-01-31;(Journal Issue). doi: 10.1010/j.ajoc.2020.010101. PMID: 12345678.'
    )
  })
  it('Publication date', () => {
    assert.equal(
      createCitation(omitProps(pubMedArticle, ['journal_volume', 'journal_issue', 'pagination'])),
      'AuthorA, AuthorB. Article Title. Journal Name. 2022-01-31. doi: 10.1010/j.ajoc.2020.010101. PMID: 12345678.'
    )
  })
  it('No provider id', () => {
    assert.equal(
      createCitation(omitProps(pubMedArticle, ['provider_id'])),
      'AuthorA, AuthorB. Article Title. Journal Name. 2022-01-31;Journal Volume(Journal Issue):101234. doi: 10.1010/j.ajoc.2020.010101.'
    )
  })
})

describe('bioRxiv Citations', () => {
  it('Publication date', () => {
    assert.equal(
      createCitation(bioRxivArticle),
      'AuthorA, AuthorB. Article Title. bioRxiv. 2022-01-31. doi: 10.1010/j.ajoc.2020.010101.'
    )
  })
  it('No publication date', () => {
    assert.equal(
      createCitation(omitProps(bioRxivArticle, ['publication_date'])),
      'AuthorA, AuthorB. Article Title. bioRxiv. doi: 10.1010/j.ajoc.2020.010101.'
    )
  })
})

describe('medRxiv Citations', () => {
  it('Publication date', () => {
    assert.equal(
      createCitation(medRxivArticle),
      'AuthorA, AuthorB. Article Title. medRxiv. 2022-01-31. doi: 10.1010/j.ajoc.2020.010101.'
    )
  })
  it('No publication date', () => {
    assert.equal(
      createCitation(omitProps(medRxivArticle, ['publication_date'])),
      'AuthorA, AuthorB. Article Title. medRxiv. doi: 10.1010/j.ajoc.2020.010101.'
    )
  })
})
