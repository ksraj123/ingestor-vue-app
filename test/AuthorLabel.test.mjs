import assert from 'assert'

import formatAuthors from '../src/modules/common/utils/formatAuthors.mjs'

const eq = (params, authors, extras = 0) => {
  const { formattedAuthors: actualAuthors, extras: actualExtras } = formatAuthors({
    ...params,
    authors: params.authors.map(name => ({ name }))
  })

  assert.equal(authors, actualAuthors)
  assert.equal(extras, actualExtras)
}

describe('Author Labels', () => {
  it('Single name author', () => {
    eq({ authors: ['Author'] }, 'Author')
    eq({ authors: ['Author'], extended: true }, 'Author')
  })

  it('First and last names', () => {
    eq({ authors: ['Last, First'] }, 'Last First')
    eq({ authors: ['Last, First'], extended: true }, 'Last First')
  })

  it('First Last Middle', () => {
    eq({ authors: ['Last, First Middle'] }, 'Last FirstM')
    eq({ authors: ['Last, First Middle'], extended: true }, 'Last FirstM')
  })

  it('Two Authors', () => {
    eq({ authors: ['LastA, FirstA MiddleA', 'LastB, FirstB MiddleB'] }, 'LastA FirstAM, LastB FirstBM')
    eq({ authors: ['LastA, FirstA MiddleA', 'LastB, FirstB MiddleB'], extended: true }, 'LastA FirstAM, LastB FirstBM')
  })

  it('Three Authors', () => {
    eq({ authors: ['LastA, FirstA MiddleA', 'LastB, FirstB MiddleB', 'LastC, FirstC MiddleC'], maxLength: 80 }, 'LastA FirstAM, LastB FirstBM, LastC FirstCM')
    eq({ authors: ['LastA, FirstA MiddleA', 'LastB, FirstB MiddleB', 'LastC, FirstC MiddleC'], extended: true }, 'LastA FirstAM, LastB FirstBM, LastC FirstCM')
  })

  it('Three Authors (hide the last name)', () => {
    eq({ authors: ['LastA, FirstA MiddleA', 'LastB, FirstB MiddleB', 'LastC, FirstC MiddleC'] }, 'LastA FirstAM, LastB FirstBM', 1)
  })

  it('Three Authors (max length = 0)', () => {
    eq({ authors: ['LastA, FirstA MiddleA', 'LastB, FirstB MiddleB', 'LastC, FirstC MiddleC'], maxLength: 0 }, 'LastA FirstAM', 2)
  })
})