#!/bin/bash

set -euxo pipefail

source $(dirname "$0")/vars
export TF_VAR_commit_hash="$(git rev-parse --short HEAD)"
export GCP_PROJECT_ID="${GCP_PROJECT_ID:-ingestor-cicd}"
export GO_CAT_GIT_URL="https://$INGESTOR_ENVIRONMENTS_SYNC_TOKEN@gitlab.com/ingestor/devops/ingestor-environments.git"

if [[ "$GCP_PROJECT_ID" == "ingestor-prod" ]]; then
  export BUILD_TYPE="prod"
else
  export BUILD_TYPE="dev"
fi
export TF_VAR_build_type=$BUILD_TYPE

# use sha256 hash of the docker container for safer deployments
# also, if the hash of the container matches the one which is already deployed on the infrastructure,
# terraform detects that there is no change in the docker container, and it can save a lot of time in deployment.
# some use cases are, when a developer edits a README.md file, and will need to wait for the entire CI to turn green
# which is a redundant deployment.
TF_VAR_platform_vue_app_docker_container="$(gcloud container images describe ${GCR_PREFIX}/${GCR_PROJECT_ID}/${DOCKER_CONTAINER_NAME}-${GCP_PROJECT_ID}-cr:${TF_VAR_commit_hash} --format='value(image_summary.fully_qualified_digest)')"
export TF_VAR_platform_vue_app_docker_container


cd scripts/gcp
git -C terraform pull || git clone --depth=1 --single-branch --branch="master" https://$INGESTOR_PULL_TOKEN@gitlab.com/ingestor/devops/terraform.git
pushd terraform/gcp/subsystems/platform/ingestor-vue-app

terraform init

ls -al environments/$GCP_PROJECT_ID.tfvar

if terraform workspace list | grep "$GCP_PROJECT_ID"; then
    terraform workspace select "$GCP_PROJECT_ID"
else
    # create a new workspace if the workspace doesn't exist
    # implies that, the gcp terraform folder is empty
    terraform workspace new "$GCP_PROJECT_ID"
fi

# as we are deploying in the cicd environment, use the cicd workspace
terraform workspace select "$GCP_PROJECT_ID"

terraform fmt -check
terraform validate

terraform apply -var-file="environments/$GCP_PROJECT_ID.tfvar" -auto-approve

terraform output

