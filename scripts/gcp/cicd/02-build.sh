#!/bin/bash

set -euxo pipefail

export BUILD_ON_DEMAND=0
export GCP_PROJECT_ID=${GCP_PROJECT_ID:-ingestor-cicd}
source $(dirname "$0")/vars
export GO_CAT_GIT_URL="https://$INGESTOR_ENVIRONMENTS_SYNC_TOKEN@gitlab.com/ingestor/devops/ingestor-environments.git"


if [ -z ${COMMIT_SHA+x} ] || [[ "$COMMIT_SHA" == "HEAD" ]]; then
    export COMMIT_SHA="$(git rev-parse --short HEAD)"
    echo "COMMIT_SHA is unset fallback, to $COMMIT_SHA"
elif [[ "$COMMIT_SHA" == "$(git rev-parse --short HEAD)" ]]; then
    # the user has provided a custom image hash
    # which we will need to deploy
    export BUILD_ON_DEMAND=1
    echo "Building $COMMIT_SHA on demand"
    git checkout "$COMMIT_SHA"
    export COMMIT_SHA="$(git rev-parse --short $COMMIT_SHA)"
fi

echo "========== BUILDING CONTAINERS ============"

if [[ "$GCP_PROJECT_ID" == "ingestor-prod" ]]; then
  export BUILD_TYPE="prod"
else
  export BUILD_TYPE="dev"
fi
set +x
for var in VUE_APP_AUTH0_DOMAIN VUE_APP_AUTH0_CLIENT_ID VUE_APP_AUTH0_AUDIENCE; do
  echo $var=$(gcloud secrets versions access latest --secret="ingestor-platform-api-$var" --project $GCP_PROJECT_ID) >> .env.$DEPLOYMENT_TYPE
done
set -x
if [[ "$DEPLOYMENT_TYPE" == "cr" ]]; then
MAIN_API_URL=$(go-cat cat -d "GCP/$GCP_PROJECT_ID/platform-api/main-api")
ILM_API_URL=$(go-cat cat -d "GCP/$GCP_PROJECT_ID/platform-api/ilm-api")
IVDR_API_URL=$(go-cat cat -d "GCP/$GCP_PROJECT_ID/platform-api/ivdr-api")
echo -e "VUE_APP_MAIN_API_URL=${MAIN_API_URL}/v1/" >> .env.$DEPLOYMENT_TYPE
echo -e "VUE_APP_IVDR_API_URL=${IVDR_API_URL}/v1/" >> .env.$DEPLOYMENT_TYPE
echo -e "VUE_APP_ILM_API_URL=${ILM_API_URL}/v1/" >> .env.$DEPLOYMENT_TYPE
cat .env.production >> .env.$DEPLOYMENT_TYPE
elif [[ "$DEPLOYMENT_TYPE" == "k8s" ]]; then
exit 0
# disable k8s build
MAIN_API_URL=$(go-cat cat -d "GCP/$GCP_PROJECT_ID/platform-k8s/main-api")
ILM_API_URL=$(go-cat cat -d "GCP/$GCP_PROJECT_ID/platform-k8s/ilm-api")
echo -e "VUE_APP_MAIN_API_URL=${MAIN_API_URL}v1/" >> .env.$DEPLOYMENT_TYPE
echo -e "VUE_APP_ILM_API_URL=${ILM_API_URL}v1/" >> .env.$DEPLOYMENT_TYPE
cat .env.production >> .env.$DEPLOYMENT_TYPE
fi


if [[ "$BUILD_TYPE" == "dev" ]]; then
  # development specific, non-secret configuration here
  echo -e "NODE_ENV=development" >> .env.$DEPLOYMENT_TYPE
fi


gcloud builds submit \
  --config=scripts/gcp/cicd/cloudbuild.yaml \
  --substitutions=_BUILD_TYPE=${BUILD_TYPE},_SUFFIX=${GCP_PROJECT_ID}-${DEPLOYMENT_TYPE},_DEPLOYMENT_TYPE=${DEPLOYMENT_TYPE},_INGESTOR_PULL_TOKEN=${INGESTOR_PULL_TOKEN},_COMMIT_SHA=${COMMIT_SHA},_GCR_PROJECT_ID=${GCR_PROJECT_ID} \
  --timeout=3h


