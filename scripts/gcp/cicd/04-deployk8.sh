#!/bin/bash

set -euxo pipefail

source $(dirname "$0")/vars
export commit_hash="$(git rev-parse --short HEAD)"
export GCP_PROJECT_ID="${GCP_PROJECT_ID:-ingestor-cicd}"
export GO_CAT_GIT_URL="https://$INGESTOR_ENVIRONMENTS_SYNC_TOKEN@gitlab.com/ingestor/devops/ingestor-environments.git"

if [[ "$GCP_PROJECT_ID" == "ingestor-prod" ]]; then
  export BUILD_TYPE="prod"
else
  export BUILD_TYPE="dev"
fi
export build_type=$BUILD_TYPE

# TODO: remove this file

exit 0
# use sha256 hash of the docker container for safer deployments
# also, if the hash of the container matches the one which is already deployed on the infrastructure,
# terraform detects that there is no change in the docker container, and it can save a lot of time in deployment.
# some use cases are, when a developer edits a README.md file, and will need to wait for the entire CI to turn green
# which is a redundant deployment.
platform_vue_app_docker_container="$(gcloud container images describe ${GCR_PREFIX}/${GCR_PROJECT_ID}/${DOCKER_CONTAINER_NAME}-${GCP_PROJECT_ID}-k8s:${commit_hash} --format='value(image_summary.fully_qualified_digest)')"
export platform_vue_app_docker_container

export GCP_PROJECT_ID="${GCP_PROJECT_ID:-ingestor-cicd}"
export K8_NAMESPACE="platform"
export K8_CLUSTER="ingestor-gke-cluster"
DNS_ZONE=$(go-cat cat -d "GCP/$GCP_PROJECT_ID/common/dns-zone")
export DNS_ZONE
export KUBECONFIG="$PWD/.kubeconfig"
gcloud container clusters get-credentials ${K8_CLUSTER} --region us-east1 --project ${GCP_PROJECT_ID}

set +x
helm repo add --username ${HELM_USER} --password ${HELM_PASS} ingestor-helm https://gitlab.com/api/v4/projects/29811284/packages/helm/stable
set -x
helm repo update 

for i in platform-vue-app; do

  helm upgrade --install "$i" -f scripts/gcp/cicd/${GCP_PROJECT_ID}.yaml ingestor-helm/application \
    --version 1.0.0 \
    --set fullnameOverride=${i} \
    --set image.repository=${platform_vue_app_docker_container} \
    -n $K8_NAMESPACE \
    --wait --timeout 10m

  # Export to environments repo.
  go-cat add --name "$i" \
    --commit-sha=$commit_hash \
    --cloud="GCP" \
    --cloud-project-id="$GCP_PROJECT_ID" \
    --type="container.googleapis.com/apps/v1" \
    --subsystem="platform-k8s" \
    --parameters="container.googleapis.com/apps/v1/namespaces=$K8_NAMESPACE,container.googleapis.com=$K8_CLUSTER" \
    --deployment-link="$(helm -n $K8_NAMESPACE status $i | grep https | sed -e 's/^[ \t]*//')"

done 
go-cat push
rm $KUBECONFIG
