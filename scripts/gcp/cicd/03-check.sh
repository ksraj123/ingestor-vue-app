#!/bin/bash
set -euxo pipefail

source $(dirname "$0")/vars
export GCP_PROJECT_ID="${GCP_PROJECT_ID:-ingestor-cicd}"

# checks if all the containers are present in the google container registry


gcloud container images list-tags \
  "$GCR_PREFIX/$GCR_PROJECT_ID/$DOCKER_CONTAINER_NAME-$GCP_PROJECT_ID-cr" | grep "$(git rev-parse --short HEAD)"
