const ilm = require('./ilm.js')
const ivdr = require('./ivdr.js')

module.exports = {
  login: {
    data: {
      access_token: 'access-token',
      id_token: 'id-token',
      scope: 'openid profile email address phone',
      expires_in: 31536000, // a year
      token_type: 'Bearer'
    }
  },
  loginFailed: {
    data: {
      detail: 'Bad Request'
    }
  },
  ilm,
  ivdr
}
