const addChar = (str, index, char = '-') => `${str.substring(0, index)}${char}${str.substring(index)}`

module.exports = {
  generateId: (str = '') => {
    let result = str.padEnd(32, '0').substring(0, 32)

    result = addChar(result, 20)
    result = addChar(result, 16)
    result = addChar(result, 12)
    result = addChar(result, 8)

    return result
  },

  generateShortId: (str = '') => {
    let result = str.padEnd(10, '0').substring(0, 10)

    result = addChar(result, 1, 'x')

    return result
  }
}
