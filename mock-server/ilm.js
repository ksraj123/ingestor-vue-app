const { generateId, generateShortId } = require('./utils.js')

const colors = [
  '#f00',
  '#ff0',
  '#f0f',
  '#0f0',
  '#0ff',
  '#00f'
]

const summary = [
  'The quick brown fox jumps over the lazy dog.',
  'Waltz, bad nymph, for quick jigs vex.',
  'Glib jocks quiz nymph to vex dwarf.',
  'Sphinx of black quartz, judge my vow.',
  'How vexingly quick daft zebras jump!',
  'The five boxing wizards jump quickly.',
  'Jackdaws love my big sphinx of quartz.',
  'Pack my box with five dozen liquor jugs.'
].join(' ')

const abdidgedSummary = `${summary.substring(0, 100)}...`

const ontologySpans = [
  'vex',
  'sphinx',
  'quartz',
  'jump'
].map(tag => {
  const spans = []
  for (let index = summary.toLowerCase().indexOf(tag); index !== -1; index = summary.toLowerCase().indexOf(tag, index + 1)) {
    spans.push({
      in_text: tag,
      predictionScore: 1,
      ontology_name: `ONT_${tag.toUpperCase()}`,
      span_start_character: index,
      span_end_character: index + tag.length
    })
  }
  return spans
})

const ontologyProps = {
  basic: {
    ontology_tag_chart: ontologySpans.map(spans => ({
      keyword: [
        {
          keyword: spans[0].in_text,
          synonyms: [
            {
              total: spans.length,
              synonym: spans[0].in_text
            }
          ],
          total: spans.length
        }
      ],
      total: spans.length,
      most_prevalent: spans[0].in_text,
      ontology_name: spans[0].ontology_name
    })),
    ontology_tags: ontologySpans.map((spans, index) => ({
      keyword: spans[0].in_text,
      synonyms: [
        spans[0].in_text
      ],
      color: colors[index % colors.length],
      ontology: spans[0].ontology_name
    }))
  },
  more: {
    spans: ontologySpans.reduce((a, b) => a.concat(b), [])
  }
}

const customFields = [
  {
    default_value: '0',
    field_name: 'opi_score',
    field_type: 'int',
    project_type: 'ILM',
    search_type: 'range',
    uid: generateShortId('opiscore')
  },
  {
    default_value: '',
    field_name: 'key_takeaways',
    field_type: 'str',
    project_type: 'ILM',
    search_type: 'full_text',
    uid: generateShortId('takeaways')
  },
  {
    default_value: '',
    field_name: 'data_highlights',
    field_type: 'str',
    project_type: 'ILM',
    search_type: 'full_text',
    uid: generateShortId('highlights')
  },
  {
    default_value: '',
    field_name: 'featured_article',
    field_type: 'boolean',
    project_type: 'ILM',
    search_type: 'term',
    uid: generateShortId('featured')
  }
]

const article1 = {
  article_id: generateId('article1'),
  article_links: [
    'https://duckduckgo.com/'
  ],
  authors: [
    {
      name: 'Sample author',
      affiliations: [
        'Affiliations go here'
      ]
    },
    {
      name: 'Sample author 2',
      affiliations: [
        'Affiliations go here'
      ]
    }
  ],
  cloud_storage_reference: 'cloud/path/to/file',
  composer_pipeline_id: generateId('composerpipelineid'),
  data_cloud_storage_path: 'cloud/path/to/file',
  data_pipeline_id: generateId('datapipelineid'),
  deliver_pipeline_id: generateId('deliverpipelineid'),
  delivery_cloud_storage_path: 'cloud/path/to/file',
  full_text_url: '',
  ia_pipeline_id: generateId('iapipelineid'),
  ingested_date: '2021-12-10',
  ingestion_version: '1.0',
  journal: 'Journal Name',
  journal_ISSN: '2021 Dec 10. doi: 12.3456/pde.12345',
  json_version: '1.0',
  language: 'eng',
  preview_excerpt: abdidgedSummary,
  project_id: generateShortId('projectid'),
  project_type: 'ilm',
  publication_date: '2021-10-15',
  starred_article: false,
  summary: summary,
  title: 'Title of article 1',
  type: 'Document',
  ...ontologyProps.basic
}

const article1More = {
  ...article1,
  biblio: [],
  data_source: 'PubMed',
  ...ontologyProps.more
}
delete article1More.project_id

const article2 = {
  ...article1,
  article_id: generateId('article2'),
  title: 'Title of article 2'
}

const article2More = {
  ...article2,
  biblio: [],
  data_source: 'PubMed',
  ...ontologyProps.more
}
delete article2More.project_id

module.exports = {
  customFields,
  article1: article1More,
  article2: article2More,
  dropdownValues: {
    data_sources: [
      'pubmed',
      'biorxiv'
    ],
    types: [
      'publication'
    ],
    page: 0,
    size: 0
  },
  projectArticlesSearch: {
    data: [article1, article2],
    count: 1000,
    data_sources: [
      [
        'PubMed'
      ]
    ],
    types: [
      'document'
    ],
    page: 1,
    size: 10
  },
  edit: {
    data: {
      uid: generateShortId('projectid'),
      project_type: 'ILM',
      project_name: 'test',
      project_requirements: null,
      project_settings: {
        image: 'https://i.imgur.com/OYxPLHw.png'
      },
      active: true,
      account: [
        {
          uid: '0x7a8abc1ed',
          account_name: 'medistrava'
        }
      ],
      custom_fields: customFields
    }
  },
  project: {
    uid: generateShortId('projectid'),
    project_type: 'ILM',
    project_name: 'ILM Project',
    project_requirements: null,
    project_settings: {
      image: 'https://i.imgur.com/OYxPLHw.png'
    },
    active: true,
    account: [
      {
        uid: '0x7a8abc1ed',
        account_name: 'medistrava'
      }
    ]
  },
  authors: [
    'Author A',
    'Author B',
    'Author C'
  ],
  journals: [
    'Journal A',
    'Journal B',
    'Journal C'
  ],
  ontologyTags: [
    'Ontology Tag 1',
    'Ontology Tag 2',
    'Ontology Tag 3'
  ]
}
