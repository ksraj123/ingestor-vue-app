const { generateId } = require('./utils.js')

const generateArticle = (override) => ({
  abstract: 'Abstract Text',
  account_id: generateId('accountid'),
  article_id: generateId('articleid'),
  article_links: [
    'https://duckduckgo.com/'
  ],
  author_auto_complete: ['Person A', 'Person B', 'Person C'].map((keyword) => ({ keyword })),
  authors: [
    ['Author A', 'Affiliation A'],
    ['Author B', 'Affiliation B'],
    ['Author C', 'Affiliation C']
  ].map(([name, affiliation]) => ({
    name,
    affiliations: [affiliation]
  })),
  biblio: [],
  cloud_storage_reference: 'cloud/path/to/file',
  composer_pipeline_id: generateId('composerPipelineid'),
  data_cloud_storage_path: 'cloud/path/to/file',
  data_pipeline_id: generateId('dataPipelineid'),
  data_source: 'PubMed',
  deliver_pipeline_id: generateId('deliverPipelineid'),
  delivery_cloud_storage_path: 'cloud/path/to/file',
  doi: '123.456.789',
  feedback_analyte_subject: true,
  feedback_human_study: true,
  feedback_intended_use: true,
  feedback_performance_study: true,
  full_text_url: '',
  ia_pipeline_id: generateId('iaPipelineid'),
  inferred_analyte_subject: true,
  inferred_human_study: true,
  inferred_intended_use: true,
  inferred_performance_study: true,
  ingested_date: '2021-10-18',
  ingestion_version: '1.0',
  journal: 'EJIFCC',
  journal_ISSN: '1111111111',
  json_version: '1.0',
  language: 'eng',
  preview_excerpt: 'Summary text goes here',
  project_id: generateId('projectid'),
  project_type: 'ivdr',
  publication_date: '2021-10-18',
  starred_article: false,
  summary: 'Summary text goes here',
  title: 'Article Title',
  type: 'Document',
  ...override
})

const db =
// Relevant articles
[1, 2].map(n => generateArticle({
  article_id: generateId(`articleid${n}-`),
  title: `Title ${n}`
})).concat(
  // Non-relevant articles
  Array.from(Array(15)).map((_, i) => i + 3)
    .map((n, i) => generateArticle({
      article_id: generateId(`articleid${n}-`),
      title: `Title ${n}`,
      inferred_analyte_subject: false,
      inferred_human_study: false,
      inferred_intended_use: i < 5,
      inferred_performance_study: i < 10,
      feedback_analyte_subject: false,
      feedback_human_study: false,
      feedback_intended_use: i < 5,
      feedback_performance_study: i < 10
    }))
)

module.exports = {
  project: {
    uid: generateId('projectid'),
    project_type: 'ivdr',
    project_name: 'IVDR Project',
    project_requirements: {},
    active: false,
    account: [
      {
        uid: '0x7a8abc1ed',
        account_name: 'medistrava',
        legacy_bot_name: 'roche-ivdr'
      }
    ],
    custom_fields: [
      {
        uid: '0x7aa1667dc',
        project_type: 'ivdr',
        field_name: 'feedback_human_study',
        default_value: 'false',
        field_type: 'boolean',
        search_type: 'range'
      }
    ]
  },
  search ({ relevant, page }) {
    const articles = db.filter(article => {
      const articleRelevant = (
        article.feedback_analyte_subject &&
          article.feedback_human_study &&
          article.feedback_intended_use &&
          article.feedback_performance_study
      )
      return relevant ? articleRelevant : !articleRelevant
    }
    )

    return {
      data: articles.slice((page - 1) * 10, page * 10),
      count: articles.length,
      page,
      size: 10
    }
  },
  update (articleId, newData) {
    const article = db.find(({ article_id: id }) => id === articleId)
    Object.entries(newData).forEach(([key, value]) => {
      article[key] = value
    })
  }
}
