const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')

const { generateId, generateShortId } = require('./utils.js')

const {
  login,
  loginFailed,
  ilm,
  ivdr
} = require('./projects.js')

const projectId = generateShortId('projectid')
const article1Id = generateId('article1')
const article2Id = generateId('article2')

const ivdrProjectId = generateId('projectid')

// https://stackoverflow.com/a/18311469
const corsMiddleware = (_, response, next) => {
  // Website you wish to allow to connect
  response.setHeader('Access-Control-Allow-Origin', 'http://localhost:8080')

  // Request methods you wish to allow
  response.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE')

  // Request headers you wish to allow
  response.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,authorization')

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  response.setHeader('Access-Control-Allow-Credentials', true)

  // Pass to next layer of middleware
  next()
}

const host = '0.0.0.0'

const send = (data = {}) => (_, response) => response.status(200).json(data)

// Port 8000 endpoints
;(() => {
  const port = 8000
  const app = express()
  app.use(corsMiddleware)
  app.use(bodyParser.json())

  app.post('/v1/login', (request, response) => {
    const { username, password } = request.body
    if (username === 'username' && password === 'password') {
      send(login)(request, response)
    } else {
      send(loginFailed)(request, response)
    }
  })
  app.get(`/v1/projects/${projectId}`, send(ilm.edit))
  app.get('/v1/projects', send({
    data: [
      ilm.project,
      ivdr.project
    ],
    total: 2,
    per_page: 1000,
    page: 1
  }))
  app.delete(`/v1/projects/${projectId}`, () => {
    send()(request, response)
  })

  app.listen(port, host, () => {
    console.log(`Mock server running on http://${host}:${port}`)
  })
})()

// 8001 endpoints
;(() => {
  const port = 8001
  const app = express()
  app.use(corsMiddleware)
  app.use(bodyParser.json())

  app.post(`/v1/projects/${projectId}`)

  app.post(`/v1/projects/${projectId}/articles/dropdown_values`, send(ilm.dropdownValues))

  app.post(`/v1/projects/${projectId}/articles/autocomplete`, (request, response) => {
    const body = request.body

    response.status(200).json(
      body.author === ''
        ? ilm.authors
        : body.journal === ''
          ? ilm.journals
          : body.ontology_tag === ''
            ? ilm.ontologyTags
            : []
    )
  })

  ;[
    [article1Id, ilm.article1],
    [article2Id, ilm.article2]
  ].forEach(([articleId, article]) => {
    app.post(`/v1/projects/${projectId}/articles/${articleId}/set_starred`, (request, response) => {
      const { starred } = request.body
      // Update the article data
      // Note: does not actually update the article data on the project page
      article.starred_article = starred
      response.status(200).json({ starred })
    })
    app.get(`/v1/projects/${projectId}/articles/${articleId}`, send(article))
  })

  app.post(`/v1/projects/${projectId}/articles/search`, send(ilm.projectArticlesSearch))

  app.listen(port, host, () => {
    console.log(`Mock server running on http://${host}:${port}`)
  })
})()

// 8006 endpoints
;(() => {
  const port = 8006
  const app = express()
  app.use(corsMiddleware)
  app.use(bodyParser.json())

  app.post(
    `/v1/projects/${ivdrProjectId}/ivdr/search`,
    (request, response) => {
      const { relevant, page } = request.body
      response.status(200).json(ivdr.search({ relevant, page }))
    }
  )

  Array.from(Array(17)).map((_, i) => i + 1)
    .map(n => generateId(`articleid${n}-`))
    .forEach(articleId => {
      app.post(
        `/v1/projects/${ivdrProjectId}/ivdr/${articleId}/ivdr_custom_fields`,
        (request, response) => {
          ivdr.update(articleId, request.body)
          response.status(200).json({})
        }
      )
    })

  app.listen(port, host, () => {
    console.log(`Mock server running on http://${host}:${port}`)
  })
})()

// 8080 endpoints
;(() => {
  if (!process.argv.includes('fe')) {
    return
  }

  const port = 8080
  const app = express()
  app.use(corsMiddleware)

  app.use(express.static(path.join(__dirname, '../dist')))

  app.get('/', function(req, res){
    res.sendFile("index.html", {root: path.join(__dirname, '../dist')})
  })

  app.listen(port, host, () => {
    console.log(`Server running http://${host}:${port}`)
  })
})()
