// https://playwright.dev/docs/test-configuration
export default {
  use: {
    // The video property, even if set to 'on', does not seem to record video
    video: 'off',
    viewport: {
      height: 600,
      width: 900
    }
  }
}