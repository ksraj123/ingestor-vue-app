# ingestor-vue-app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## Production deploying

With the new changes, files are no longer expected to be served buy an additional service. 

Now the Docker image serves the app in `8080`. `docker build -t ingestor/app-services/ingestor-vue-app  . --build-arg INGESTOR_PULL_TOKEN=$INGESTOR_PULL_TOKEN`

This token replaces dependency to ssh `git+ssh` to `git+https` using a deploy token from ingestor-vue-storybooks.

### Webserver

The webserver on deplyment has been changed from Express to Nginx.
You can continue to use express on local docker by building the image with ```--target server```





### Mock Server

For automated testing, or in case the ingestor-platform-api is unable to make requests, a local server can be used to test UI/UX functionality without an official back-end.

To run the mock server:

* Ensure the ingestor-platform-api is not currently running

* Ensure ports 8000, 8001, and 8006 are available

* Update/create `.env.local` and define this env variable:
  ```
  VUE_APP_AUTH0_DOMAIN=mock-server
  ```

* In a separate terminal, run `npm run mock-server`
  * This starts an expressJS server running on the requisite ports
  * This service should be left running until you are ready to shut down the mock server, which you can shut down via `^C`

* In a different terminal, run `npm run serve`



### UI Tests

To run ui tests:
* Follow the steps above to run the `mock-server`
* Run `npm run ui-test`

Note that UI tests ONLY work with the mock-server. This is to ensure that tests can run in isolation without hard dependencies on external code/authentication.
